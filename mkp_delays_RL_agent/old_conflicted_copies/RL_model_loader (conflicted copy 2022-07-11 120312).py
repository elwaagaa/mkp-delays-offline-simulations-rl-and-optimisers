"""
Script to load trained RL model for the MKP delay RL agent 
"""

import numpy as np
import matplotlib.pyplot as plt
import pickle
import time 
import gym

from stable_baselines3 import PPO, A2C # DQN coming soon
from stable_baselines3 import TD3
from stable_baselines3.common.noise import NormalActionNoise
from stable_baselines3.common.env_checker import check_env
from mkpdelays_env_RL import MKPOptEnv

# Initiate and load model 
algorithm = 'TD3'
n_steps = 50000
dtc = 250
model_str = "plots_and_data/model _{}_steps_{}_ns_dtc_{}".format(n_steps, dtc, algorithm)

# Initiate environment
env =  MKPOptEnv(dtc=dtc)   # also include batch spacing time separation

# ------------------- DEFINE TRAINING ALGORITHM and LOAD MODEL --------------------------------------
# Train the agent - TD3, here we use action noise
if algorithm == 'TD3':
    model = TD3.load(model_str)

# Train the agent - A2C, and clock the training 
if algorithm == 'A2C':
    model = A2C.load(model_str)

# Train agent PPO
if algorithm == 'PPO':
    model = PPO.load(model_str)
# ------------------------------------------------------------------------------------


# Test the loaded agent
obs = env.reset()
n_steps_test = 20
for step in range(n_steps_test):
  action, _ = model.predict(obs, deterministic=True)
  print("Step {}".format(step + 1))
  print("Action: ", action)
  obs, reward, done, info = env.step(action)
  print("Reward: {}".format(reward))
  if done:
    print("Goal reached!", "reward=", reward)
    break

# Final beam position, also to generate Twiss 
final_beam_pos = env.mkp_delays.step(env.inv_norm_data(action))  



