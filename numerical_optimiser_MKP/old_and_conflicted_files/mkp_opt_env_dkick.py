"""
Optimisation environment to find the ideal time shift vector dtau for the electrical switches 
of the MKPs in the SPS injection in order to minimise the horizontal beam oscillations in the SPS 

This version does not perform a MADX Twiss command for every iteration, but minimises the kick deviation dx' 
with respect to today
"""

import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import sys
from cpymad.madx import Madx

#Import accelerator physics library with functions
#sys.path.append('/home/ewaagaa/cernbox2/documents/waagaard_tech_studentship/python/functions')  #for office computer 
sys.path.append('C:/Users/Elias/cernbox2/documents/waagaard_tech_studentship/python/functions') 
import acc_phy_lib_elias 


class MKPOptEnv_dkick():
    
    #Load optics, voltage-to-kick converted data, and corresponding time data
    def __init__(self, optics):
        self.optics = optics
        self.mkp_kick_data = np.genfromtxt('data_and_sequences/sps_lhcpilot_kick_data.txt') 
        self.time_data = np.genfromtxt('data_and_sequences/sps_lhcpilot_time_data.txt') 
        self.ref_timestamp = 0   #timestamp of flat top kick in ns,  use a third into the flat top for MKP module 16 (meaning exactly at the midpoint - 1/6*width)
        self.dtc = 900  #time shift in ns between injected batch and ciruclating batch in the SPS
        self.start_reset()
        
    #Method to calculate oscillations of the injected and circulating beam (c) for a given timeshift vector for the MKP switches 
    def step(self, dtau):

        #Check at what time to evaluate the MKP kick for the injected beam, and for the circulating beam
        self.t = self.ref_timestamp + dtau[-1]  #new general time stamp 
        self.tc = self.ref_timestamp + dtau[-1] - self.dtc  #new general time stamp for circulating beam
        self.dt_general = dtau[-1]  #general time shift 
        self.t_inds = []
        self.tc_inds = []
        for i in range(8):  #8 switches in total to iterate over 
            t_ind = np.argwhere(self.time_data==(self.t+dtau[i] - ((self.t+dtau[i]) % 2)))  #find closest even index corresponding to this time 
            tc_ind = np.argwhere(self.time_data==(self.tc+dtau[i] - ((self.tc+dtau[i]) % 2)))  
            self.t_inds.append(t_ind)
            self.tc_inds.append(tc_ind)

        
        #Iterate over the modules, check the time shift for each switch, and add the kicks for the injected and circulating beam
        self.new_kicks = []
        self.new_kicks_c = []
        switch_count = 0
        for i in range(16):
            if (((i) % 2) == 0) and (i!=0):  #after every two steps, change electrical switch
                switch_count += 1  
            kick = self.mkp_kick_data[i][self.t_inds[switch_count]]
            kick_c = self.mkp_kick_data[i][self.tc_inds[switch_count]]
            self.new_kicks.append(kick)
            self.new_kicks_c.append(kick_c)
        
        #Define a vector with new MKP kicks, for the injected and circulating beam
        self.mkp_kicks = np.empty(4)
        self.mkp_kicks[0] = np.sum(self.new_kicks[:5])
        self.mkp_kicks[1] = np.sum(self.new_kicks[5:10])
        self.mkp_kicks[2] = np.sum(self.new_kicks[10:12])
        self.mkp_kicks[3] = np.sum(self.new_kicks[12:16])
        self.mkp_kicks_c = np.empty(4)
        self.mkp_kicks_c[0] = np.sum(self.new_kicks_c[:5])
        self.mkp_kicks_c[1] = np.sum(self.new_kicks_c[5:10])
        self.mkp_kicks_c[2] = np.sum(self.new_kicks_c[10:12])
        self.mkp_kicks_c[3] = np.sum(self.new_kicks_c[12:16])
      
        
        
        #Update the MKP kicks through the MADX globals
        self.madx.globals['kmkpa11931'] = self.mkp_kicks[0] 
        self.madx.globals['kmkpa11936'] = self.mkp_kicks[1] 
        self.madx.globals['kmkpc11952'] = self.mkp_kicks[2]
        self.madx.globals['kmkp11955'] = self.mkp_kicks[3] 
        self.madx2.globals['kmkpa11931'] = self.mkp_kicks_c[0] 
        self.madx2.globals['kmkpa11936'] = self.mkp_kicks_c[1] 
        self.madx2.globals['kmkpc11952'] = self.mkp_kicks_c[2]
        self.madx2.globals['kmkp11955'] = self.mkp_kicks_c[3] 
        
        """
        self.madx.sequence['short_injection_and_some_sps'].elements['mkpa.11931'].kick = self.mkp_kicks[0] 
        self.madx.sequence['short_injection_and_some_sps'].elements['mkpa.11936'].kick = self.mkp_kicks[1] 
        self.madx.sequence['short_injection_and_some_sps'].elements['mkpc.11952'].kick = self.mkp_kicks[2] 
        self.madx.sequence['short_injection_and_some_sps'].elements['mkp.11955'].kick = self.mkp_kicks[3] 
        self.madx2.sequence['reduced_circulating_sps'].elements['mkpa.11931'].kick = self.mkp_kicks_c[0] 
        self.madx2.sequence['reduced_circulating_sps'].elements['mkpa.11936'].kick = self.mkp_kicks_c[1] 
        self.madx2.sequence['reduced_circulating_sps'].elements['mkpc.11952'].kick = self.mkp_kicks_c[2] 
        self.madx2.sequence['reduced_circulating_sps'].elements['mkp.11955'].kick = self.mkp_kicks_c[3] 
        """
        
       
        print("MKP kicks injected beam: {}".format(self.mkp_kicks))
        print("MKP kicks circulating beam: {}".format(self.mkp_kicks_c))
        #print("MKP kicks circulating beam: {}".format(self.mkp_kicks_c))

