"""
RL test wrapper to create fake MKP delays environment, testing out an RL agent with Stable Baselines 3
--> this versions uses environment that implements the relative action
"""

import numpy as np
import matplotlib.pyplot as plt
import pickle
import time 
import gym

from stable_baselines3 import PPO, A2C # DQN coming soon
from stable_baselines3 import TD3
from stable_baselines3.common.noise import NormalActionNoise
from stable_baselines3.common.env_checker import check_env
from mkpdelays_env_RL_delta_action import MKPOptEnv  # import environment using relative action

# Initiate some values and flags 
save_plots = True
n_steps = 5000
dtc = 250
sigmoid_start =  3.  # sigmoid(-5) ~= 0.1 --> if argument is negative, then randomization between [arg, 0], otherwise between [0, arg]
check_env_flag = False 
algorithm = 'TD3'
run_nr = 3
seed = run_nr 
save_str = "{}_run_{}_no_boundary_penalty".format(algorithm, run_nr)

# Initiate environment
env =  MKPOptEnv(dtc=dtc, sigmoid_start=sigmoid_start)   # also include batch spacing time separation
if check_env_flag:
    check_env(env, warn=True)  # If the environment don't follow the interface, an error will be thrown

# Perform a reset command
obs = env.reset()
#env.render()

#"""
# ------------------- DEFINE TRAINING ALGORITHM --------------------------------------
# Train the agent - TD3, here we use action noise
if algorithm == 'TD3':
    start = time.time()
    n_actions = env.action_space.shape[-1]
    action_noise = NormalActionNoise(mean=np.zeros(n_actions), sigma=0.05 * np.ones(n_actions))
    model = TD3("MlpPolicy", env, verbose=1, seed=seed, action_noise=action_noise, buffer_size=100000)  # need to slightly limit buffer size 
    model.learn(total_timesteps=n_steps)
    end = time.time()

# Train the agent - A2C, and clock the training 
if algorithm == 'A2C':
    start = time.time()
    model = A2C('MlpPolicy', env, seed=seed, verbose=1).learn(n_steps)
    end = time.time()

# Train agent PPO
if algorithm == 'PPO':
    start = time.time()
    model = PPO('MlpPolicy', env, seed=seed, verbose=1).learn(total_timesteps = n_steps)
    end = time.time()
# ------------------------------------------------------------------------------------


# Test the trained agent
obs = env.reset()
n_steps_test = 20
for step in range(n_steps_test):
  action, _ = model.predict(obs, deterministic=True)
  print("Step {}".format(step + 1))
  print("Action: ", action)
  obs, reward, done, info = env.step(action)
  print("Reward: {}".format(reward))
  if done:
    print("Goal reached!", "reward=", reward)
    break

# Final beam position, also to generate Twiss 
final_beam_pos = env.mkp_delays.step(env.inv_norm_data(action))  

print("\nFinal real action: {}".format(env.log_data["real_actions"][-1]))
print("\nTraining with {} steps lasted for {:.2f} seconds!\n".format(n_steps, end - start))


# Generate figure of the waveforms 
fig = env.render("matplotlib_figures")
if save_plots:
    fig[0].savefig('plots_and_data_delta_action/waveforms_{}_steps_{}_ns_dtc_{}.png'.format(n_steps, dtc, save_str), dpi=250)


#Also plot reward over time 
fig1 = plt.figure(figsize=(10,7))
ax = fig1.add_subplot(1, 1, 1)  # create an axes object in the figure
ax.yaxis.label.set_size(20)
ax.xaxis.label.set_size(20)
plt.xticks(fontsize=14) 
plt.yticks(fontsize=14)
ax.plot(env.episode_data["reward_sum"])
ax.set_xlabel("Training episode")
ax.set_ylabel("Reward")
if save_plots:
    fig1.savefig('plots_and_data_delta_action/agent_reward_{}_steps_{}_ns_dtc_{}.png'.format(n_steps, dtc, save_str), dpi=250)

# plot resulting beam oscillations
fig2 = plt.figure(figsize=(10,7))
env.mkp_delays.plot_oscillations(fig2)
if save_plots:
    fig2.savefig('plots_and_data_delta_action/injection_oscillations_{}_steps_{}_ns_dtc_{}.png'.format(n_steps, dtc, save_str), dpi=250)



# Save model
if save_plots:
    model.save("plots_and_data_delta_action/model _{}_steps_{}_ns_dtc_{}".format(n_steps, dtc, save_str))

# Serialize and save data
if save_plots:
    with open(
        "plots_and_data_delta_action/episode_data_{}_steps_{}_ns_dtc_{}.pickle".format(
            n_steps, dtc, save_str
        ),
        "wb",
    ) as handle:
        pickle.dump(
            env.episode_data, handle, protocol=pickle.HIGHEST_PROTOCOL
        )
#"""