# Offline simulation and optimisation of MKP delays - numerical optimisation and Reinforcement learning

Just like the repository [MKP delay optimiser](https://gitlab.cern.ch/elwaagaa/mkp_delay_optimiser) for online optimisation, this repository contains the environments to optimise the injection kicker magnet (MKP) time delays for the Super Proton Synchrotron (SPS) injection system. However, these environments are adapted for offline execution outside of the CERN Control Centre, using either **numerical optimisers** or **Reinforcement Learning (RL) agents**. The ideal waveform for powering the fast-pulsed MKP magnets (often operating in the nanosecond regime) has a steep rise time and a flat top. We want the injected beam time stamp to be placed on the flat top where there is full deflection from the injection kicker, and the already circulating beam should arrive before when there is no field. The separation in time between the injected and circulating beam is called *batch spacing*. The ideal waveform of an injection kicker can be seen below to the left, and real typical waveforms to the right. Due to flat top ripples, the flat top is not fully flat.  

![mkp_waveform_profile.png](./mkp_waveform_profile.png)

The LHC beams require tight batch spacing - often 200-500 ns - meaning that one has to find the ideal balance between giving full deflection to the injected beam, and as little deflection as possible to the circulating beam. The main objective is to minimize the squared sum of the injection oscillation amplitude of the circulating and injected beam, by finding the ideal time delays for the MKP timings. In total, there are eight individual time shifts and one general time shift for the MKP magnet modules. My Master's thesis on the subject can be found on [CDS](https://cds.cern.ch/record/2813209), with more technical details, also on the numerical optimisers. 

Just like the MKP delay optimiser for use in the CCC, the optimisation environments follow the GYM environment API, and the the standards of the [optimisation GUI](https://gitlab.cern.ch/be-op-ml-optimization/acc-app-optimisation) already used for many optimisers in the CCC. The main methods, common for all environments, are:
- `__init()__` is the constructor, where we select the batch spacing, which MKP delays to shift, the time delay limit, and the beam type. Depending on whether we have a numerical optimiser or RL, the waveform data is called in different ways in the constructor.
- `reset()` is called at the beginning of an episode, returning the initial observation of the state 
- `render()` visualizes how the injection time stamps are changed in action.
- `step(action)` takes an action with the environment, returns the next observation, the immediate reward, and a boolean whether the episode has finished or not. 

## NXCALS data extracter 

[NXCALS](https://nxcals-docs.web.cern.ch/current/) is a logging system used at CERN for storing and accessing data for different accelerator parameters. This folder contains two Python notebooks to exctract and convert the data we need:
- `Extracting-MKP-waveform.ipynb` demonstrates with an example how to extract MKP waveform data from NXCALS into a SPARK dataframe. The MKP parameter names can be found on [CCDE](https://ccde.cern.ch/). Then historical MKP waveform data can be downloaded for different periods and for different selectors (beam types). 
- `NXCALS_data_extraction/MKP_waveform_data_flattop_boxcar_fit.ipynb` instructs on how to convert voltage of the MKP modules to actual beam deflection, by fitting boxcar functions to the present flat tops where the present kicks are known. 

## Numerical optimiser

The numerical optimiser acts via two main units: 
- the optimisation environment class contained in `mkp_opt_env.py`, which contains the `step()` and `reset()` method and all the information from the environment. The environment also has a method `step_dkick(action)`, which does not use MAD-X simulations (which can potentially be heavy) in the optimisation but instead loads the present beam deflection for the MKP modules and minimizes any deviations from these, for the given waveform data. The environment also contains methods to calculate the resulting horizontal action-angle variable $`J_x`$ after injection. As we have $`\varepsilon_x = \langle J_x \rangle`$ for the emittance, we strive to keep the increase of $`J_x`$ as small as possible. Some interesting aspects of preserving the emittance can be found in the [CAS 2017 slides from Verena Kain](https://indico.cern.ch/event/451905/contributions/2159070/attachments/1423256/2191040/EmittancePreservation_v1.pdf).
- the wrapper `runOpt_mkp_waveform.py` is a script that initiates the environment and optimises its `step()` method using either the Powell or Nelder-Mead algorithm from the SciPy libray, especially using the [`scipy.optimize.minimize`](https://docs.scipy.org/doc/scipy/reference/generated/scipy.optimize.minimize.html) method.

For some historical MKP waveform data of batch spacing = 250 ns, the timestamps found by the optimiser correspond also to the lowest total increase in $`J_x`$ for both the circulating and injected beam, as seen below. 

![optimised_MKP_timestamps_and_Jx.png](./optimised_MKP_timestamps_and_Jx.png)

## Reinforcement Learning (RL) agent

The main issue with the numerical optimiser is the number of iterations needed to reach the termination criteria for a minimum, usually several hundreds iterations. The rise time of the MKP waveforms may change over time, meaning that the delays have to be re-optimised, for which expensive beam time is needed. Instead, a well-trained RL agent poses an alternative that can re-adjust the MKP delays in only a few iterations instead of several hundred. An illustrative scheme of the RL agent is shown below. 

![RL_agent.png](./RL_agent.png)


In our case, the RL is *model-free*, meaning that the agent has to interact with the environment to learn about it. At every step of interaction, the agent observes the **state** of the environment and decides what **action** to take. The environment state changes, and the agent receives a **reward** $`r`$ depending on the new state of the environment. The agent uses a **policy** to determine what action to take, where we use a stochastic parametrized policy $`\pi_{\theta}(s, a)`$, which is a computable function depending on the parameter $`\theta`$. In deep RL, we use neural networks to approximate $`\pi_{\theta}(s, a)`$. An **episode** (or trajectory) is a sequence of states $`s`$ and actions $`a`$ in the world, often with a maximum number of steps. The objective is to minimize the **return** $`R`$

```math
R = \sum_{t = 0}^T \gamma^t r_t
```
where $`r_t`$ is the reward given for each step, and $`\gamma \in (0, 1)`$ is the discount factor. A full introduction on RL developing these concepts further can be found on the [OpenAI: Spinning Up website](https://spinningup.openai.com/en/latest/spinningup/rl_intro.html). 

In this context, the state $`s`$ is a vector of 50-60 points sampled over the rise time of each MKP module, with 16 modules in total. We also add the maximum oscillation amplitude of the injected and circulating beam to the state. Thus, the **state space** contains 800-900 variables. The eight individual MKP delays + the general MKP delay constitute the **action space**. Both the state space and action space are normalized to $`[-1, 1]`$ for each variable. The action is normalized over the boundaries, which we set to 100 ns for the individual delays and 750 ns for the general delays. Any action outside $`[-1, 1]`$ will be clipped. We define the reward as 

```math
r_t = -\Big[ \bar{x}^2 + \bar{x}_c^2  + (\bar{x} - \bar{x}_c)^2\Big]
```

where $`\bar{x}`$ and $`\bar{x}_c`$ are the maximum oscillation amplitudes of the injected and circulating beams. The episode length is set to maximum 100 steps, or until the target reward of 0.0005 has been reached, which corresponds to a satisfactory level of beam oscillations recorded from the numerical optimiser. For every episode, MKP waveform data with different rise time profiles is generated by multiplying the rise time interval of the extracted data by the sigmoid function $`\sigma(z)`$, where $`z \in [-5 \times \textrm{np.random.rand()}, 5]`$, which will generate randomly shaped MKP rise times for every episode. 

We use the model-free RL algorithms provided by `stable-baselines-3`, which are divided into two main areas: 
- **Policy optimisation**: policy is directly represented as $`\pi_{\theta}(s, a)`$, parameters $`\theta`$ are optimised directly e.g. by gradient descent. Optimization is *on-policy*.
    - Algorithms: Synchronous Advantage Actor Critic (**A2C**) and Proximal Policy Optimisation (**PPO**)
- **Q-learning**: methods learns an approximator $`Q_{\theta}(s, a)`$ for the optimal action-value function. Optimization is *off-policy*.
    - Algorithms: Twin Delayed DDPG (**TD3**) learns policy and Q-function, so also includes aspect of policy optimisation

The wrapper `RL_wrapper.py` contains the input information for the RL agent training (steps per episode, RL algorithm, batch spacing) and trains the agent, to finally test the trained agent. The RL agent environment is contained in `mkpdelays.py`, which contains the `step()` and `reset()` methods. The subclass `fake_mkp_delays_RL.py` is used by the environment to store and randomize the historical waveform data, and from which the state is observed containing the MKP rise times and injection oscillations from a MAD-X simulation (thus simulating a "fake" BPM readout). There exists a corresponding test version with the additional name string `delta_action`, which aims at training an agent by moving in relative steps instead of taking absolute normalized action in $`[-1, 1]`$. This version aims at being even more directly deployable in the CCC even after only having been trained on simulations, but so far it does not perform as well as an agent taking direct action, rather often trying to move outside the specified bounds.

Trained models can be directly loaded from `RL_model_loader.py`. In addition, the scripts `RL_training_reward_loader.py` and `RL_training_initial_final_reward_plotter.py` load and plot the returns and rewards throughout the episodes. The script `Performace_plot_generator_RL_models.py` generates performance plots, similar to those plotted for numerical optimisers in my thesis, to generate cumulative distributions to compare how well each RL algorithm performs for 100 different randomly generated rise times. So far, the most promising algorithm seems to be A2C, whose average of 5 training sessions (seeds) is shown below, where a steady and uniform improvement can be seen.    

![RL_training.png](./RL_training.png)

The plot below compares the performance between a trained RL agent with A2C and a numerical optimiser using the Powell method, and how many steps it took for them to get there. The batch spacing $`dt_c`$ is 250 ns. The trained agent is seemingly much faster and achieves the same result!  

![optimiser_vs_RL_agent.png](./optimiser_vs_RL_agent.png)
